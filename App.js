import React from "react";
import AppNavigation from "./src/AppNavigator/AppNavigation";

const App = () => {
  return <AppNavigation />;
};

export default App;
