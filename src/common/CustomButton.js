import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from "react-native";
import React from "react";

const CustomButton = ({ title,  onPress,bg }) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.btn, { backgroundColor: bg }]}
      onPress={onPress}
    >
      <Text style={{ color: 'white' }}>{title}</Text>
    </TouchableOpacity>
  );
};

export default CustomButton;
const styles = StyleSheet.create({
  btn: {
    width: '15%',
    height: 30,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 30,
    borderRadius: 20,
  },
});
