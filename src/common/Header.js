import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
} from "react-native";
import React from "react";
import { useNavigation } from "@react-navigation/native";
const { height, width } = Dimensions.get("window");

const Header = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.header}>
      <View style={styles.container}>
        <TouchableOpacity style={styles.btn} onPress={()=>{navigation.openDrawer()}}>
          <Image source={require("../images/menu.png")} style={styles.icon} />
        </TouchableOpacity>
        <Text style={styles.title}>Women</Text>
      </View>
      <View style={styles.iconContainer}>
        <TouchableOpacity style={styles.btn} onPress={() => {}}>
          <Image
            source={require("../images/search.png")}
            style={[styles.icon, { width: 25, height: 25 }]}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn} onPress={() => {}}>
          <Image
            source={require("../images/heart.png")}
            style={[styles.icon, { width: 25, height: 25 }]}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.btn} onPress={() => {}}>
          <Image
            source={require("../images/shopping-bag.png")}
            style={[styles.icon, { width: 40, height: 40 }]}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Header;
const styles = StyleSheet.create({
  header: {
    width: width,
    height: 65,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 15,
  },
  btn: {
    width: 40,
    height: 40,
    justifyContent: "center",
    alignItems: "center",
    marginRight:10
  },
  icon: {
    width: 30,
    height: 30,
    tintColor: "black",
  },
  title: {
    color: "black",
    fontSize: 16,
  },
  iconContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
});
