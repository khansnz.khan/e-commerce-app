import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Keyboard,
} from "react-native";
import React, { useEffect, useState } from "react";
import Home from "./tabs/Home";
import User from "./tabs/User";

const HomeScreen = () => {
  const [selectTab, setSelectTab] = useState(0);

  return (
    <View style={styles.container}>
      {selectTab == 0 ? <Home /> : <User />}
      <View style={styles.bottomView}>
        <TouchableOpacity
          style={styles.bottomTab}
          onPress={() => {
            setSelectTab(0);
          }}
        >
          <Image
            source={
              selectTab == 0
                ? require("../images/home-active.png")
                : require("../images/home-inactive.png")
            }
            style={styles.bottomTabIcon}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.bottomTab}
          onPress={() => {
            setSelectTab(1);
          }}
        >
          <Image
            source={
              selectTab == 0
                ? require("../images/user-inactive.png")
                : require("../images/user-active.png")
            }
            style={styles.bottomTabIcon}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bottomView: {
    position: "absolute",
    bottom: 0,
    width: "100%",
    height: 70,
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  bottomTab: {
    width: "20%",
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  bottomTabIcon: {
    width: 24,
    height: 24,
  },
});
