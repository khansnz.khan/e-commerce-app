import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import Header from "../../common/Header";

const Home = () => {
  const [product, setProduct] = useState([]);
  useEffect(() => {
    getProduction();
  }, []);
  const getProduction = () => {
    fetch("https://fakestoreapi.com/products")
      .then((res) => res.json())
      .then((json) => {
        setProduct(json);
      });
  };
  return (
    <View style={StyleSheet.container}>
      <Header />
      <View style={styles.filterContainer}>
        <Text style={{ fontWeight: "bold" }}>{product.length} items</Text>
        <View style={{ flexDirection: "row" }}>
          <TouchableOpacity style={styles.btn} onPress={() => {}}>
            <Image
              source={require("../../images/filter.png")}
              style={styles.icon}
            />
          </TouchableOpacity>
          <Text style={{ fontWeight: "bold" }}>Filter</Text>
        </View>
      </View>
      <FlatList
        data={product}
        numColumns={2}
        keyExtractor={(item) => item.id}
        renderItem={({ item, index }) => {
          return (
            <TouchableOpacity
              activeOpacity={1}
              onPress={() => {}}
              style={styles.productItem}
            >
              <Image source={{ uri: item.image }} style={styles.item} />
              <View style={{ flexDirection: "row", marginTop: 10 }}>
                <Image
                  source={require("../../images/star.png")}
                  style={styles.star}
                />
                <Text>{item.rating.rate}</Text>
              </View>
              <View style={styles.detail}>
                <Text style={styles.name}>
                  {item.title.length > 12
                    ? item.title.substring(0, 12) + "..."
                    : item.title}
                </Text>
                <Text style={styles.desc}>
                  {item.description.length > 15
                    ? item.description.substring(0, 15) + "..."
                    : item.description}
                </Text>
                <Text style={styles.price}> {"\u20B9" + item.price}</Text>
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

export default Home;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  productItem: {
    width: "47%",
    backgroundColor: "#fff",
    marginTop: 5,
    marginHorizontal: 5,
    padding: 15,
  },
  item: {
    width: 150,
    height: 150,
  },
  name: {
    fontSize: 18,
    fontWeight: "600",
    marginLeft: 0,
  },
  desc: {
    marginLeft: 0,
  },
  price: {
    color: "black",
    fontSize: 18,
    fontWeight: "800",
    marginLeft: 0,
    marginTop: 5,
  },
  filterContainer: {
    marginHorizontal: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  btn: {
    width: 20,
    height: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: 10,
  },
  icon: {
    width: 20,
    height: 20,
    tintColor: "black",
  },
  detail: {
    marginTop: 15,
    justifyContent: "flex-start",
  },
  star: {
    height: 15,
    width: 15,
  },
});
