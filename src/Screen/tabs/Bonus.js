import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";
import Progress from "./Progress";
import Confirm from "./Confirm";

const Bonus = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.bonus}> Bonus in 2021</Text>
      <View style={styles.horizontalLine} />
      <Progress />
      <View style={styles.horizontalLine} />
      <Confirm />
    </View>
  );
};

export default Bonus;
const styles = StyleSheet.create({
  container: {
    height: "80%",
    width: "100%",
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    backgroundColor: "white",
    marginTop: 20,
  },
  bonus: {
    fontWeight: "700",
    color: "#666666",
    marginVertical: 15,
    paddingLeft: 15,
    fontSize: 16,
  },
  horizontalLine: {
    height: 2,
    width: "90%",
    //   justifyContent: "center",
    alignSelf: "center",
    backgroundColor: "#666666",
  },
});
