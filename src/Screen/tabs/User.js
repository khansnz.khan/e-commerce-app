import { View, Text, StyleSheet } from "react-native";
import React from "react";
import CustomButton from "../../common/CustomButton";
import Bonus from "./Bonus";

const User = () => {
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: "row", justifyContent: "space-evenly" }}>
        <CustomButton title={"All"} bg='#101213'/>
        <CustomButton title={"Recent"} bg='#666666' />
        <CustomButton title={"Old"} bg='#666666' />
        <CustomButton title={"filter"} bg='#666666' />
      </View>
      <Bonus/>
    </View>
  );
};

export default User;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#333333",
  },
});
