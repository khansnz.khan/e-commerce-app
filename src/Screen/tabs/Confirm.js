import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";

const Confirm = () => {
  return (
    <View style={{ flexDirection: "row", paddingLeft: 15, marginVertical: 10 }}>
      <View style={{ width: "10%" }}>
        <Image
          source={require("../../images/check-mark.png")}
          style={styles.icon}
        />
      </View>
      <View style={{ width: "90%" }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingRight: 15,
            marginRight: 15,
          }}
        >
          <View>
            <Text style={{ fontSize: 18, color: "Black", fontWeight: "bold" }}>
              #JFHJD44556D
            </Text>
            <Text>14th May</Text>
          </View>
          <View>
            <Text>status</Text>
            <Text style={{ color: "green", fontWeight: "600" }}>Confirm</Text>
          </View>
        </View>
        <View style={{ marginVertical: 5 }}>
          <Text>Amount</Text>
          <Text style={{ fontWeight: "bold" }}>$2.00</Text>
        </View>
      </View>
    </View>
  );
};

export default Confirm;
const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    marginTop: 10,
  },
});
