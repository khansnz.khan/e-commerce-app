import { View, Text, StyleSheet, Image } from "react-native";
import React from "react";

const Progress = () => {
  return (
    <View style={{ flexDirection: "row", paddingLeft: 15, marginVertical: 10 }}>
      <View style={{ width: "10%" }}>
        <Image source={require("../../images/clock.png")} style={styles.icon} />
      </View>
      <View style={{ width: "90%" }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            paddingRight: 15,
            marginRight: 15,
          }}
        >
          <View>
            <Text style={{ fontSize: 18, color: "Black", fontWeight: "bold" }}>
              #JFHJD44556D
            </Text>
            <Text>14th May</Text>
          </View>
          <View>
            <Text>status</Text>
            <Text style={{ color: "#FFDB58", fontWeight: "600" }}>
              In progress
            </Text>
          </View>
        </View>
        <View style={{ marginVertical: 5 }}>
          <Text>Amount</Text>
          <Text style={{ fontWeight: "bold" }}>$2.00</Text>
        </View>
        <View style={{ flexDirection: "row", marginVertical: 5 }}>
          <Text>Amount Info</Text>
          <View
            style={[
              styles.horizontalLine,
              {
                width: 240,
                alignSelf: "flex-start",
                marginTop: 10,
                marginLeft: 10,
              },
            ]}
          />
        </View>
        <Text style={{ fontSize: 18, fontWeight: "bold" }}>Lorem Ipsum</Text>
        <View style={{ flexDirection: "row", marginVertical: 5 }}>
          <Text>Deductions</Text>
          <View
            style={[
              styles.horizontalLine,
              {
                width: 250,
                alignSelf: "flex-start",
                marginTop: 10,
                marginLeft: 10,
              },
            ]}
          />
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginRight: 40,
          }}
        >
          <View>
            <Text>Service</Text>
            <Text>$2.00</Text>
          </View>
          <View
            style={{
              height: 15,
              width: 1,
              backgroundColor: "#666666",
              alignSelf: "center",
            }}
          />
          <View>
            <Text>TDS</Text>
            <Text>$2.00</Text>
          </View>
          <View
            style={{
              height: 15,
              width: 1,
              backgroundColor: "#666666",
              alignSelf: "center",
            }}
          />
          <View>
            <Text>IMPS FEE</Text>
            <Text>$2.00</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Progress;
const styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
    marginTop: 10,
  },
});
